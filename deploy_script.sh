#!/bin/bash

set -e

echo "Starting deployment..."

echo "Pulling latest code from GitLab..."
git pull origin main

echo "Building Docker image..."
sudo docker build -t nginx:latest .

echo "Stopping existing containers..."
sudo docker stop $(sudo docker ps -a -q)

echo "Starting new container..."
sudo docker run -d -p 8080:80 nginx:latest

echo "Deployment completed successfully."
